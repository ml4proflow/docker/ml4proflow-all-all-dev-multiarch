FROM arm32v7/python:3-bullseye
RUN /bin/sh -c "apt-get update && apt-get install -y jupyter-server npm ca-certificates nodejs && npm install -g --unsafe-perm node-red && cd /root && git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/docker/ml4proflow-all-all-dev-multiarch.git . && pip install -r requirements.txt && mkdir .node-red && cd .node-red && npm install ../src/ml4proflow-nodered"
